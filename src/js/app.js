import $ from 'jquery';
import Swiper from 'swiper';
import '../../src/js/lib/jquery.event.move';
import '../../src/js/lib/jquery.twentytwenty';
import '../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';
import '../../node_modules/jquery-mask-plugin/dist/jquery.mask.min';
import '../../node_modules/jquery-validation/dist/jquery.validate.min';
import '../../node_modules/jquery-popup-overlay/jquery.popupoverlay';
import '../../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min';

var swiper = new Swiper('.how-we-works__slider_wrapper-slider.swiper-container ', {
  simulateTouch: false,
  allowTouchMove: false,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});
var swiperV = new Swiper('.how-we-works__slider_container.swiper-container', {
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  }, navigation: {
    nextEl: '.swiper-button-next-two',
    prevEl: '.swiper-button-prev-two',
  },
});
$(document).scroll(function() {
  // - - - - SCROOL - - - - OPEN
  if($(this).scrollTop() > 70) {
    $('.header').addClass('scroll');
  }
  else {
    $('.header').removeClass('scroll');
  }
  if($(this).scrollTop() > 900) {
    $('.up-scroll').addClass('scroll');
  }
  else {
    $('.up-scroll').removeClass('scroll');
  }
  // - - - - SCROLL - - - - CLOSE
});

$(document).ready(function() {

  $('.nav li a').on('click', function(event) {
    var target = $(this.getAttribute('href'));

    if (target.length) {
      event.preventDefault();
      $('html, body').stop().animate({
        scrollTop: target.offset().top  - 40
      }, 1000);
    }
  });
  $('.up-scroll').on('click', function(event) {
    var target = $(this.getAttribute('href'));

    if (target.length) {
      event.preventDefault();
      $('html, body').stop().animate({
        scrollTop: target.offset().top
      }, 1000);
    }
  });
  $('.button-scroll').on('click', function(event) {
    var target = $(this.getAttribute('href'));

    if (target.length) {
      event.preventDefault();
      $('html, body').stop().animate({
        scrollTop: target.offset().top  - 40
      }, 1000);
    }
  });
  $('.how-we-works__slider_twentytwenty').twentytwenty();
  $('.nav__button').click(function() {
    $(this).toggleClass('active')
    $('.nav').toggleClass('active');
    $('.callback').toggleClass('active');
  });

  $('.modal').popup({
    transition: 'all 0.3s',
    outline: true, // optional
    focusdelay: 400, // optional
    vertical: 'top', //optional
    closebutton: true
  });

  $('input[type="tel"]').mask('+38 (000) 000-00-00');
  jQuery.validator.addMethod('phoneno', function(phoneNumber, element) {
    return this.optional(element) || phoneNumber.match(/\+[0-9]{2}\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/);
  }, 'Введите Ваш телефон');

  $('.form').each(function(index, el) {
    $(el).addClass('form-' + index);

    $('.form-' + index).validate({
      rules: {
        name: 'required',
        agree: 'required',
        tel: {
          required: true,
          phoneno: true
        }
      },
      messages: {
        name: 'Неправильно введенное имя',
        tel: 'Неправильно введен телефон',
        agree: 'Нужно соглашение на обработку данных',
      },
      submitHandler: function(form) {
        var t = $('.form-' + index).serialize();
        ajaxSend('.form-' + index, t);
      }
    });
  });
  function ajaxSend(formName, data) {
    jQuery.ajax({
      type: 'POST',
      url: 'sendmail.php',
      data: data,
      success: function() {
        $('.modal').popup('hide');
        $('#thanks').popup('show');
        setTimeout(function() {
          $(formName).trigger('reset');
        }, 2000);
      }
    });
  };

  var mapControl = document.getElementById('map') !== null ? initMap() : null;

});

$(window).on('load',function() {
  $('.how-we-works__slider_review').mCustomScrollbar();
});

$(window).resize(function() {
  if ($(window).width() > 991) {
    $('.nav').slideDown();
    $('nav__button').removeClass('active');
  }
});
